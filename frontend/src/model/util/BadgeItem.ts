import ClubItemDto from "../dto/ClubItemDto";

export default class BadgeItem {
    constructor(public title: string, public description: string, public label: string,
                public backgroundUrl: string, public href: string) {
    }

    static fromClubInfo(clubInfo: ClubItemDto): BadgeItem {
        const backgroundUel = clubInfo.avatar ? `${window.location.origin}/api/clubs/image?id=${clubInfo.avatar}` : '';
        return new BadgeItem(clubInfo.title, clubInfo.address, `Средняя цена: ${clubInfo.priceMean}`,
            backgroundUel, `/club?clubId=${clubInfo.id}`);
    }
}
