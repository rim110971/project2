import {GameVariant} from "../util/GameVariant";
import {AppointmentModel} from "@devexpress/dx-react-scheduler";
import {fromObject} from "../../utils/utils";
import {DayOfWeek} from "../util/DayOfWeek";


export type Pricing = Record<GameVariant, number>;

/**
 * Один слот в расписании.
 */
export class ClubScheduleResponseItem {
    constructor(public id: string,
                public startDateTime: string,
                public endDateTime: string,
                public pricing: Pricing,
                public daysOfWeek: DayOfWeek[]) {
    }

    public static fromObject(o: object): ClubScheduleResponseItem {
        return fromObject(o, ClubScheduleResponseItem);
    }

    toAppointment(): AppointmentModel {
        let description = "";
        if (this.pricing.snooker) description += `Снукер: ${this.pricing.snooker} Р`;
        if (this.pricing.pool) description += `\nПул: ${this.pricing.pool} Р`;
        if (this.pricing.russian) description += `\nРусский: ${this.pricing.russian} Р`;
        return {
            startDate: this.startDateTime,
            endDate: this.endDateTime,
            title: 'Meeting',
            id: this.id,
            description: description,
            item: this
        };
    }
}
