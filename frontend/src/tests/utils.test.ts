import {UserLogin} from "../model/users/UserLogin";
import {Role} from "../model/users/Role";
import {addHours, extractDateFromISO, isBefore} from "../utils/timeUtils";
import {checkIfTimeAllowedForBooking} from "../utils/bookingTimeUtils";

describe('Test clone function', () => {
    test('Clone UserLogin', () => {
        const userLogin = UserLogin.fromObject({email: "email", name: "username", role: Role.CLIENT}).clone();
        expect(userLogin).toBeInstanceOf(UserLogin);
        expect(userLogin.email).toBe('email');
        expect(userLogin.name).toBe('username');
        expect(userLogin.role).toBe(Role.CLIENT);

    });

    test('Creates UserLogin from raw object', () => {
        const userLogin = UserLogin.fromObject({email: "email", name: "username", role: Role.CLIENT});
        expect(userLogin).toBeInstanceOf(UserLogin);
        expect(userLogin.email).toBe('email');
        expect(userLogin.name).toBe('username');
        expect(userLogin.role).toBe(Role.CLIENT);
    });
});

describe('checkIfTimeAllowedForBooking Test', () => {
    const today = new Date();
    const tomorrow = new Date();
    tomorrow.setDate(today.getDate() + 1);
    const yesterday = new Date();
    yesterday.setDate(today.getDate() - 1);
    test('Should return true for time > current + TIME_RESERVED_IN_HOURS', () => {
        const givenDate = extractDateFromISO(today.toISOString());
        const result = checkIfTimeAllowedForBooking(givenDate, '17:00', '12:00');
        expect(result).toBeTruthy();
    });
    test('Should return false for time < current + TIME_RESERVED_IN_HOURS', () => {
        const givenDate = extractDateFromISO(today.toISOString());
        const result = checkIfTimeAllowedForBooking(givenDate, '15:00', '12:00');
        expect(result).toBeFalsy();
    });
    test('Should return true for date > today', () => {
        const givenDate = extractDateFromISO(tomorrow.toISOString());
        const result = checkIfTimeAllowedForBooking(givenDate, '13:00', '12:00');
        expect(result).toBeTruthy();
    });


    test("Should add hours to time", () => {
        const givenTime = '10:33';
        const when2HoursAdded = addHours(givenTime, 2);
        expect(when2HoursAdded).toBe('12:33');
    });
});

describe('isBefore test', () => {
   test("Should return true for hours difference", () => {
       const givenBeforeDate = '10:00';
       const givenAfterDate = '11:00';
       const whenIsBeforeCalled = isBefore(givenBeforeDate, givenAfterDate);
       expect(whenIsBeforeCalled).toBeTruthy();
   });
    test("Should return false for hours difference", () => {
        const givenBeforeDate = '12:00';
        const givenAfterDate = '11:00';
        const whenIsBeforeCalled = isBefore(givenBeforeDate, givenAfterDate);
        expect(whenIsBeforeCalled).toBeFalsy();
    });
    test("Should return true for minutes difference", () => {
        const givenBeforeDate = '10:00';
        const givenAfterDate = '10:05';
        const whenIsBeforeCalled = isBefore(givenBeforeDate, givenAfterDate);
        expect(whenIsBeforeCalled).toBeTruthy();
    });
    test("Should return false for minutes difference", () => {
        const givenBeforeDate = '11:01';
        const givenAfterDate = '11:00';
        const whenIsBeforeCalled = isBefore(givenBeforeDate, givenAfterDate);
        expect(whenIsBeforeCalled).toBeFalsy();
    });
});


describe('Date test', () => {
    test('Parse pure date', () => {
        expect(new Date('2022-05-14').toISOString()).toBe('2022-05-14T00:00:00.000Z');
    });
    test('Compare dates', () => {
        expect(new Date('2022-05-14T00:00:00.000Z').getDate()).toBe(new Date('2022-05-14T15:45:40.000Z').getDate());
    });
});