import React, {useEffect, useState} from 'react';
import {Navigate, useSearchParams} from 'react-router-dom';
import Title from "../../components/Title";
import ClubPageLayout from "../../components/layout/ClubPageLayout";
import {Card, Link, Typography} from "@mui/material";
import EditableList from "../../components/EditableList";
import {ClubProfile} from "../../model/club/ClubProfile";
import {ImagesViewer} from "../../components/photosInput/ImagesViewer";
import Centered from '../../components/Centered';
import {getClubProfile} from "../../api/profile";
import {useInfo, useSuccess, useWarn} from "../../hooks/logging";
import {Cities} from "../../model/util/Cities";
import ImageDTO from "../../model/dto/ImageDTO";
import PageButton from "../../components/PageButton";
import {DescriptionField} from "../../components/DescriptionField";
import {styled} from "@mui/material/styles";
import classes from './ClubDetailedPage.module.scss';
import {BookingDialog} from "./BookingDialog";
import {defaultTablesInfo} from "../../model/club/TablesCounts";
import {Booking} from "../../model/bookings/Booking";
import {addBookingClient} from "../../api/booking";
import Conditional from "../../components/Conditional";
import {useSelector} from "react-redux";
import {GuestManager} from "../../rbac/managers";
import getCredentials from "../../hooks/getCredentials";

const StyledCard = styled(Card)(({theme}) => ({
    background: theme.card.background,
    padding: '10px',
    width: '100%'
}));

const clubDefaultProfile = new ClubProfile("1@1", "1", "club", "ATEGWDASV WW", "a", Cities.Ekb,
    [], defaultTablesInfo, ['a']);


export default function ClubDetailedPage() {
    const [params] = useSearchParams();
    const credentials = useSelector(getCredentials);
    const [openBookingDialog, setOpenBookingDialog] = useState(false);
    const clubId = params.get('clubId');
    const warn = useWarn();
    const success = useSuccess();

    const [clubProfile, setClubProfile] = useState(clubDefaultProfile);

    useEffect(() => {
        if (clubId)
            getClubProfile(clubId as string)
                .then(p => setClubProfile(ClubProfile.fromObject(p)))
                .catch(warn);
    }, []);

    if (!clubId) {
        return <Navigate to='/empty_club_id'/> //TODO: provide page for not_found
    }

    function pinImage(image: ImageDTO) {
        setClubProfile(clubProfile.withPhotos(ImageDTO.pinImage(clubProfile.photos, image)));
    }

    function makeBooking(booking: Booking) {
        addBookingClient(booking, clubId as string)
            .then(() => success("Бронь успешно создана"))
            .catch(warn);
    }

    const useSecondStub = clubProfile.title.length % 2;

    const workTime = useSecondStub ? (<>
        ПН-ПТ:14:00-23:00
        <br/>
        СБ-ВС: 10:00-23:00
    </>) : (<>
        Пн-Чт: 13:00-22:00
        <br/>
        Пт-Вс: 11:00-22:00
    </>) ;

    const priceList = useSecondStub ? (<>
        Понедельник-Четверг 350р/час
        <br/>
        Пятница-Воскресенье 450р/час
    </>) : (<>
        Пн-Чт 200р/час
        <br/>
        Пт-Вс до 18:00: 300р/час
        <br/>
        Пт-Вс после 18:00: 350р/час
    </>);

    return (
        <Centered>
            <div className={classes.clubDetailedPage}>
                <Title title={`Информация о клубе ${clubProfile.title}`}/>
                <ClubPageLayout style={{minWidth: 'auto'}}
                                leftColumn={
                                    (<>
                                        <Typography variant='h6'>
                                            Контактные данные
                                        </Typography>
                                        <DescriptionField title="Электронная почта">
                                            {clubProfile.email}
                                        </DescriptionField>
                                        <DescriptionField title="Номер телефона">
                                            {clubProfile.phoneNumber}
                                        </DescriptionField>
                                        <DescriptionField title="Адрес">
                                            {clubProfile.address}
                                        </DescriptionField>
                                        <DescriptionField title="Режим работы">{/*TODO: fetch from back*/}
                                            <div style={{marginLeft: "15px"}}>
                                                {workTime}
                                            </div>
                                        </DescriptionField>
                                        <DescriptionField title="Тарифы">{/*TODO: fetch from back*/}
                                            <div style={{marginLeft: "15px"}}>
                                                {priceList}
                                            </div>
                                        </DescriptionField>
                                        <br/>
                                        <Typography variant='h6'>
                                            Количество столов для:
                                        </Typography>
                                        <Conditional condition={clubProfile.inventory?.russian as unknown as boolean}>
                                            <DescriptionField title="Русского бильярда">
                                                {clubProfile.inventory?.russian}
                                            </DescriptionField>
                                        </Conditional>
                                        <Conditional condition={clubProfile.inventory?.pool as unknown as boolean}>
                                            <DescriptionField title="Американского пула">
                                                {clubProfile.inventory?.pool}
                                            </DescriptionField>
                                        </Conditional>
                                        <Conditional condition={clubProfile.inventory?.snooker as unknown as boolean}>
                                            <DescriptionField title="Снукера">
                                                {clubProfile.inventory?.snooker}
                                            </DescriptionField>
                                        </Conditional>
                                        <br/>
                                        <EditableList title="Дополнительные услуги" inputPlaceholder='Добавить услугу'
                                                      items={clubProfile.additionalServices} readonly/>
                                        <br/>
                                        <StyledCard>
                                            <Typography paragraph>{clubProfile.description}</Typography>
                                        </StyledCard>
                                        <br/>
                                        <PageButton onClick={() => setOpenBookingDialog(true)} disabled={!credentials}>
                                            Забронировать стол
                                        </PageButton>
                                        <GuestManager>
                                            Для бронирования стола необходимо
                                            <Link href='/authentication'>авторизоваться</Link>
                                        </GuestManager>
                                    </>)
                                }
                                rightColumn={<ImagesViewer onImageClick={pinImage} images={clubProfile.photos}/>}
                />
            </div>
            <BookingDialog clubId={clubId} open={openBookingDialog} onClose={() => setOpenBookingDialog(false)}
                           onSubmit={makeBooking}/>
        </Centered>);

}

