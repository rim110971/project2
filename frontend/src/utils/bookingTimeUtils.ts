import {addDays, addHours, extractDateFromISO, extractTimeDate, extractTimeFromISO, isBefore} from "./timeUtils";


export const maxAvailableDate = extractDateFromISO(addDays(new Date(), 14).toISOString());

export const TIME_RESERVED_IN_HOURS = 4;
const todayDate = new Date().getDate();

/**
 *
 * @param dateISOString
 * @param timeToCheck in format 'HH:mm'
 * @param currentTime in format 'HH:mm'
 */
export function checkIfTimeAllowedForBooking(dateISOString: string, timeToCheck: string,
                                             currentTime: string = extractTimeDate(new Date())): boolean {
    const date = new Date(dateISOString).getDate();
    if (todayDate > date) {
        throw new Error('The date before today is not allowed!')
    } else if (date === todayDate) {
        const minAllowedBookingTime = addHours(currentTime, TIME_RESERVED_IN_HOURS);
        return isBefore(minAllowedBookingTime, timeToCheck);
    }
    return true;
}