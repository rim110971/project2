package com.platform.projapp.controller;

import com.platform.projapp.dto.response.body.MessageResponseBody;
import com.platform.projapp.error.ErrorConstants;
import com.platform.projapp.service.ProcessingException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalDefaultExceptionHandler {
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<MessageResponseBody> handleException() {
        return ResponseEntity.badRequest().body(MessageResponseBody.of(ErrorConstants.INCORRECT_DATA.getMessage()));
    }

    @ExceptionHandler(ProcessingException.class)
    public ResponseEntity<MessageResponseBody> handleProcessingException(ProcessingException processingException) {
        return ResponseEntity.badRequest().body(MessageResponseBody.of(processingException.getMessage()));
    }
}
