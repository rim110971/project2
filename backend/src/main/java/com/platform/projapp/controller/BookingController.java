package com.platform.projapp.controller;

import com.platform.projapp.dto.BookingItem;
import com.platform.projapp.dto.ClubLink;
import com.platform.projapp.dto.UserShortBody;
import com.platform.projapp.dto.response.FullBookingSlot;
import com.platform.projapp.dto.response.GeneralResponse;
import com.platform.projapp.model.AccessRole;
import com.platform.projapp.model.BilliardTypes;
import com.platform.projapp.service.BookingService;
import com.platform.projapp.service.ClubService;
import com.platform.projapp.service.ProcessingException;
import com.platform.projapp.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/bookings")
@RequiredArgsConstructor
public class BookingController {
    private final UserService userService;
    private final ClubService clubService;
    private final BookingService bookingService;

    @GetMapping
    public ResponseEntity<GeneralResponse<List<BookingItem>>> getBookings(
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam("date") LocalDate date,
            @RequestHeader(name = "Authorization") String token) {
        log.info("getClubBookings for {}", date);
        var user = userService.parseAndFindByJwt(token);
        var club = clubService.findByAdminId(user.getId());
        var response = new GeneralResponse<List<BookingItem>>();
        final List<BookingItem> responseBody = bookingService.getBookingByClubId(club.getId(), date);
        response.setData(responseBody);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/my")
    public ResponseEntity<GeneralResponse<List<BookingItem>>> getMyBooking(@RequestHeader(name = "Authorization") String token) {
        var user = userService.parseAndFindByJwt(token);
        var response = new GeneralResponse<List<BookingItem>>();
        final List<BookingItem> responseBody = bookingService.getBookingByUserId(user.getId());
        response.setData(responseBody);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/club")
    public ResponseEntity<GeneralResponse<List<BookingItem>>> postClubBooking(
            @RequestHeader(name = "Authorization") String token, @RequestBody BookingItem request) {
        var user = userService.parseAndFindByJwt(token);
        var club = clubService.findByAdminId(user.getId());
        try {
            var response = new GeneralResponse<List<BookingItem>>();
            final List<BookingItem> stubResponseBody = bookingService.makeClubBooking(user, request, club);
            response.setData(stubResponseBody);
            return ResponseEntity.ok(response);
        } catch (ProcessingException e) {
            var response = new GeneralResponse<List<BookingItem>>();
            return ResponseEntity.status(400).body(response.withError(e.getMessage()));
        }
    }

    @PostMapping("/client")
    public ResponseEntity<?> postClientBooking(
            @RequestHeader(name = "Authorization") String token,
            @RequestParam("clubId") Long clubId, @RequestBody BookingItem request) {
        var user = userService.parseAndFindByJwt(token);
        var club = clubService.findById(clubId);
        try {
            bookingService.makeClientBooking(user, request, club);
            return ResponseEntity.ok().build();
        } catch (ProcessingException e) {
            var response = new GeneralResponse<List<BookingItem>>();
            return ResponseEntity.status(400).body(response.withError(e.getMessage()));
        }
    }

    @PutMapping
    public ResponseEntity<GeneralResponse<List<BookingItem>>> putBooking(
            @RequestHeader(name = "Authorization") String token, @RequestBody BookingItem request) {
        var user = userService.parseAndFindByJwt(token);
        switch (user.getRole()) {
            case ROLE_CLIENT -> {}
            case ROLE_CLUB -> {}
        }
        var response = new GeneralResponse<List<BookingItem>>();
        final List<BookingItem> stubResponseBody = List.of();//TODO: replace with real implementation
        response.setData(stubResponseBody);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping
    public ResponseEntity<GeneralResponse<List<BookingItem>>> deleteBooking(
            @RequestHeader(name = "Authorization") String token, @RequestParam(name = "id") Long id) {
        var user = userService.parseAndFindByJwt(token);
        var date = bookingService.getBookingById(id).getDate();
        bookingService.cancelBooking(id);
        if (user.getRole().equals(AccessRole.ROLE_CLUB)) {
            var club = clubService.findByAdminId(user.getId());
            var response = new GeneralResponse<List<BookingItem>>();
            final List<BookingItem> responseBody = bookingService.getBookingByClubId(club.getId(), date);
            response.setData(responseBody);
            return ResponseEntity.ok(response);
        } else {
            var response = new GeneralResponse<List<BookingItem>>();
            final List<BookingItem> responseBody = bookingService.getBookingByUserId(user.getId());
            response.setData(responseBody);
            return ResponseEntity.ok(response);
        }
    }

    @GetMapping("/full")
    public ResponseEntity<GeneralResponse<List<FullBookingSlot>>> getFullBooking(
            @RequestParam(name = "clubId", required = false) Long clubId,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam("date") LocalDate date,
            @RequestParam("gameVariant") String gameVariant) {
        var response = new GeneralResponse<List<FullBookingSlot>>();
        log.info("getFullBooking for {} and {} and {}", date, clubId, gameVariant);
        response.setData(List.of(
                new FullBookingSlot(date.atTime(6, 0, 0), date.atTime(10, 0, 0), "Нерабочее время", "#AAAAAA"),
                new FullBookingSlot(date.atTime(10, 0, 0), date.atTime(23, 0, 0), "Есть свободные столы", "#83BAFF"),
                new FullBookingSlot(date.atTime(23, 0, 0), date.atTime(23, 59, 59), "Нерабочее время", "#AAAAAA")
        ));
        return ResponseEntity.ok(response);
    }
}
