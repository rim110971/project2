package com.platform.projapp.controller;

import com.platform.projapp.dto.response.GeneralResponse;
import com.platform.projapp.dto.response.body.ClubResponseShortBody;
import com.platform.projapp.dto.response.body.ClubsResponseEntity;
import com.platform.projapp.service.ClubService;
import com.platform.projapp.service.ScheduleService;
import com.platform.projapp.service.UserService;
import com.platform.projapp.service.mappers.ClubMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ClubController {
    private final ClubService clubService;
    private final ClubMapper clubMapper;

    @GetMapping("/api/clubs")
    public ResponseEntity<?> getClubs(@RequestParam(name = "city") String city,
                                      @RequestParam(name = "filters", required = false) String filters,
                                      @PageableDefault(size = 9) Pageable pageable) {
        var response = new GeneralResponse<>();
        var page = clubService.findAllByCityAndFilters(city, pageable, filters);
        var clubs = new ArrayList<ClubResponseShortBody>();
        for (var club : page.toList()) {
            var priceMean = clubService.getPriceMean(club.getId());
            clubs.add(new ClubResponseShortBody(club, (int)Math.ceil(priceMean / 50) * 50));
        }

        ClubsResponseEntity clubsResponseEntity = ClubsResponseEntity.of(
                pageable.getPageNumber(),
                page.getTotalElements(),
                clubs
        );

        return ResponseEntity.ok(response.withData(clubsResponseEntity));
    }

    @GetMapping("/api/club")
    public ResponseEntity<?> getClub(@RequestParam(name = "clubId") Long clubId) {
        var response = new GeneralResponse<>();
        try {
            var club = clubService.findById(clubId);
            var admin = club.getAdmin();
            var inventory = clubService.getClubInventory(club.getId());
            var responseBody = clubMapper.clubToProfileDTO(club);
            responseBody.setEmail(admin.getEmail());
            responseBody.setPhoneNumber(admin.getPhoneNumber());
            responseBody.setInventory(inventory);
            responseBody.getPhotos().forEach(p -> {
                if (p.getId().equals(club.getAvatar())) {
                    p.setAvatar(true);
                }
            });
            response = response.withData(responseBody);
            return ResponseEntity.ok(response);
        }
        catch (Exception e) {
            log.error("Error getting club with id {}", clubId, e);
            return ResponseEntity.status(404).body(response.withError(e.getMessage()));
        }
    }
}
