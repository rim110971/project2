package com.platform.projapp.controller;

import com.platform.projapp.service.ImageService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/clubs/image")
public class ImageController {
    private final ImageService imageService;

    @GetMapping
    public byte[] getImage(@RequestParam(name = "id") String id) {
        return imageService.getImageById(id);
    }
}
