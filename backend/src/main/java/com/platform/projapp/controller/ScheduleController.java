package com.platform.projapp.controller;

import com.platform.projapp.dto.request.ScheduleItemRequest;
import com.platform.projapp.dto.response.ClubScheduleResponseItem;
import com.platform.projapp.dto.response.GeneralResponse;
import com.platform.projapp.model.BilliardTypes;
import com.platform.projapp.service.ClubService;
import com.platform.projapp.service.PriceService;
import com.platform.projapp.service.ScheduleService;
import com.platform.projapp.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/api/schedule")
@RequiredArgsConstructor
public class ScheduleController {
    private final UserService userService;
    private final ClubService clubService;
    private final ScheduleService scheduleService;
    private final PriceService priceService;


    @GetMapping
    public ResponseEntity<GeneralResponse<List<ClubScheduleResponseItem>>> getSchedule(
            @RequestHeader(name = "Authorization") String token) {
        var user = userService.parseAndFindByJwt(token);
        var club = clubService.findByAdminId(user.getId());
        var schedules = scheduleService.getScheduleOfClub(club);
        var response = new GeneralResponse<List<ClubScheduleResponseItem>>();
        response.setData(schedules); //ВАЖНО: ДЛЯ НОРМАЛЬНОЙ РАБОТЫ ФРОНТЫ, ЕСЛИ УКАЗАНО НЕСКОЛЬКО ДНЕЙ ДНЕЙ НЕДЕЛИ, НА КАЖДЫЙ ДЕНЬ НЕДЕЛИ ДОЛЖЕН БЫТЬ СВОЙ ОБЪЕКТ С КОРРЕКТНОЙ ДАТОЙ !!!
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<GeneralResponse<List<ClubScheduleResponseItem>>> postSchedule(
            @RequestBody ScheduleItemRequest request, @RequestHeader(name = "Authorization") String token) {
        log.info("postSchedule with {}", request);
        var user = userService.parseAndFindByJwt(token);
        var club = clubService.findByAdminId(user.getId());
        var data = scheduleService.addScheduleItem(request, club);
        var response = new GeneralResponse<List<ClubScheduleResponseItem>>();
        response.setData(data); //ВАЖНО: ДЛЯ НОРМАЛЬНОЙ РАБОТЫ ФРОНТЫ, ЕСЛИ УКАЗАНО НЕСКОЛЬКО ДНЕЙ ДНЕЙ НЕДЕЛИ, НА КАЖДЫЙ ДЕНЬ НЕДЕЛИ ДОЛЖЕН БЫТЬ СВОЙ ОБЪЕКТ С КОРРЕКТНОЙ ДАТОЙ !!!
        return ResponseEntity.ok(response);
    }

    @PutMapping
    public ResponseEntity<GeneralResponse<List<ClubScheduleResponseItem>>> putSchedule(
            @RequestBody ScheduleItemRequest request, @RequestHeader(name = "Authorization") String token) {//TODO: implement
        log.info("putSchedule with {}", request);
        var user = userService.parseAndFindByJwt(token);
        var club = clubService.findByAdminId(user.getId());
        var response = new GeneralResponse<List<ClubScheduleResponseItem>>();
        response.setData(List.of(new ClubScheduleResponseItem("1",
                        LocalDate.now().atTime(10, 0, 0),//даты должны формироваться на основании дней дней недели и и текущей даты
                        LocalDate.now().atTime(20, 0, 0),
                        Map.of(BilliardTypes.POOL, 150,
                                BilliardTypes.SNOOKER, 250,
                                BilliardTypes.RUSSIAN, 200),
                        List.of(LocalDate.now().getDayOfWeek().ordinal(), LocalDate.now().plusDays(1).getDayOfWeek().ordinal())),
                new ClubScheduleResponseItem("1", //тот же айдишник (т.к. это единый отрезок)
                        LocalDate.now().plusDays(1).atTime(10, 0, 0),//но время соответствует дню недели
                        LocalDate.now().plusDays(1).atTime(20, 0, 0),
                        Map.of(BilliardTypes.POOL, 150,
                                BilliardTypes.SNOOKER, 250,
                                BilliardTypes.RUSSIAN, 200),
                        List.of(LocalDate.now().getDayOfWeek().ordinal(), LocalDate.now().plusDays(1).getDayOfWeek().ordinal())))); //TODO: !!! ВАЖНО: ДЛЯ НОРМАЛЬНОЙ РАБОТЫ ФРОНТЫ, ЕСЛИ УКАЗАНО НЕСКОЛЬКО ДНЕЙ ДНЕЙ НЕДЕЛИ, НА КАЖДЫЙ ДЕНЬ НЕДЕЛИ ДОЛЖЕН БЫТЬ СВОЙ ОБЪЕКТ С КОРРЕКТНОЙ ДАТОЙ !!!
        return ResponseEntity.ok(response);
    }

//    @DeleteMapping
//    public ResponseEntity<?> deleteSchedule(@RequestParam(name = "id") Long id) {
//        var response = new GeneralResponse<>();
//        return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(null);
//    }
}
