package com.platform.projapp.controller;

import com.platform.projapp.dto.ProfileDTO;
import com.platform.projapp.dto.response.GeneralResponse;
import com.platform.projapp.model.AccessRole;
import com.platform.projapp.service.ClubService;
import com.platform.projapp.service.UserService;
import com.platform.projapp.service.mappers.ClubMapper;
import com.platform.projapp.service.mappers.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/profile")
@RequiredArgsConstructor
public class UserController {
    private final ClubService clubService;
    private final UserService userService;
    private final ClubMapper clubMapper;
    private final UserMapper userMapper;
    @GetMapping
    public ResponseEntity<?> getUser(@RequestHeader(name = "Authorization") String token) {
        var user = userService.parseAndFindByJwt(token);
        var response = new GeneralResponse<>();
        if (user.getRole().equals(AccessRole.ROLE_CLIENT)) {
            response.withData(userMapper.userToShortBodyDto(user));
        } else {
            var club = clubService.findByAdminId(user.getId());
            var inventory = clubService.getClubInventory(club.getId());
            var responseBody = clubMapper.clubToProfileDTO(club);
            responseBody.setEmail(user.getEmail());
            responseBody.setPhoneNumber(user.getPhoneNumber());
            responseBody.setInventory(inventory);
            response = response.withData(responseBody);
        }
        return ResponseEntity.ok(response);
    }

    @PutMapping
    public ResponseEntity<?> putUser(@RequestBody ProfileDTO request) {
        var response = new GeneralResponse<>();
        return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(null);
    }
}
