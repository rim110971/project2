package com.platform.projapp.repository;

import com.platform.projapp.model.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalTime;
import java.util.List;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
    List<Schedule> findAllByClubId(Long id);

    @Query(value = "SELECT s FROM Schedule s WHERE s.startTime <= :start AND s.endTime >= :end")
    List<Schedule> findAllByBookingTime(@Param("start") LocalTime start, @Param("end") LocalTime end);
}

