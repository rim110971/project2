package com.platform.projapp.repository;

import com.platform.projapp.dto.response.body.ClubResponseShortBody;
import com.platform.projapp.model.Club;
import liquibase.pro.packaged.P;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Repository
public interface ClubRepository extends JpaRepository<Club, Long> {
    Club findByTitle(String title);

    Boolean existsByTitleAndCity(String title, String city);

    @Query("SELECT club FROM Club club WHERE club.city = :city")//TODO: use filters and calculate priceMean
    Page<Club> findAllByCity(@Param("city") String city, Pageable pageable);

    @Query("SELECT c FROM Club c WHERE  c.admin.id = :id")
    Club findByAdminId(@Param("id") Long id);

    @Query(value = "SELECT AVG(p.price) from clubs c " +
            "LEFT JOIN schedules s ON c.club_id = s.club_id " +
            "LEFT JOIN prices p ON s.schedule_id = p.schedule_id " +
            "WHERE c.club_id = :id",
            nativeQuery = true
    )
    Optional<Double> getPriceMean(@Param("id") Long id);
}

