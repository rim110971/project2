package com.platform.projapp.repository;

import com.platform.projapp.model.BilliardTypes;
import com.platform.projapp.model.Booking;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long> {
    List<Booking> getAllBookingByUserId(Long userId, Sort sort);

    List<Booking> getAllBookingByClubIdAndDate(Long clubId, LocalDate date, Sort sort);

    @Query(value = "SELECT SUM(b.count) FROM Booking b WHERE b.id = :clubId AND b.date = :date AND b.variant = :variant" +
            " AND ((b.startTime between :start AND :end) OR (b.endTime between :start AND :end))")
    Optional<Integer> getCountBusyTables(
            @Param("clubId") Long clubId,
            @Param("date") LocalDate date,
            @Param("start") LocalTime startTime,
            @Param("end") LocalTime endTime,
            @Param("variant") BilliardTypes type
    );
}

