package com.platform.projapp.repository;

import com.platform.projapp.model.Price;
import com.platform.projapp.model.PriceId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PriceRepository extends JpaRepository<Price, PriceId> {
    @Query("SELECT p FROM Price p WHERE  p.id.scheduleId = :id")
    List<Price> findAllByScheduleId(@Param("id") Long id);
}

