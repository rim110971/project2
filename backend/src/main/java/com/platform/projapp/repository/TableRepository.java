package com.platform.projapp.repository;

import com.platform.projapp.model.BilliardTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Repository;

import javax.persistence.Tuple;
import java.util.List;

@Repository
public interface TableRepository extends JpaRepository<BilliardTable, Long> {
    @Query(value = "SELECT tabs.type, COUNT(tabs.id) FROM BilliardTable tabs " +
            "WHERE tabs.clubId = :clubId GROUP BY tabs.type")
    List<Tuple> getClubInventory(@Param("clubId") Long clubId);

    List<BilliardTable> findAllByClubId(Long clubId);
}
