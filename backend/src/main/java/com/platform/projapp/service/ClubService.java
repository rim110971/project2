package com.platform.projapp.service;

import com.platform.projapp.dto.ImageDTO;
import com.platform.projapp.dto.request.RegisterOrUpdateClubRequest;
import com.platform.projapp.dto.response.body.ClubResponseShortBody;
import com.platform.projapp.model.BilliardTypes;
import com.platform.projapp.model.Club;
import com.platform.projapp.repository.ClubRepository;
import com.platform.projapp.repository.ImageRepository;
import com.platform.projapp.service.mappers.ClubMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class ClubService {
    private final ClubRepository clubRepository;
    private final ImageRepository imageRepository;
    private final ClubMapper clubMapper;
    private final UserService userService;
    private final TableService tableService;

    public Club findById(Long id) {
        return clubRepository.findById(id).orElse(null);
    }

    public Club findByTitle(String title) {
        return clubRepository.findByTitle(title);
    }

    public Club findByAdminId(Long id) {
        return clubRepository.findByAdminId(id);
    }

    public Map<BilliardTypes, Long> getClubInventory(Long clubId)  {
        return tableService.getClubInventory(clubId);
    }

    public void addClub(RegisterOrUpdateClubRequest registerRequest) {
        if (registerRequest.getPassword() != null) {
            var club = clubMapper.dtoToEntity(registerRequest);
            var admin = userService.findByUserName(registerRequest.getEmail());
            club.setAdmin(admin);
            club.setAdditionalServices(String.join(";", registerRequest.getAdditionalServices()));
            clubRepository.save(club);
            persistImages(registerRequest.getPhotos(), club);
            tableService.addTablesToClub(registerRequest.getInventory(), club.getId());
        }
    }

    private void persistImages(List<ImageDTO> imageDTOs, Club club) {
        var images = clubMapper.dtosToEntitys(imageDTOs);
        images.forEach(i -> i.setClubId(club.getId()));
        club.setAvatar(imageDTOs.stream().filter(ImageDTO::isAvatar).map(ImageDTO::getId).findFirst().orElse(null));
        imageRepository.saveAll(images);
    }

    public Page<Club> findAllByCityAndFilters(String city, Pageable pageable, String filters) {//TODO: implement filters
        return clubRepository.findAllByCity(city, pageable);
    }

    public double getPriceMean(Long clubId) {
        return clubRepository.getPriceMean(clubId).orElse(0.0);
    }
}