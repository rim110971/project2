package com.platform.projapp.service.mappers;

import com.platform.projapp.dto.BookingItem;
import com.platform.projapp.model.Booking;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface BookingMapper {
    @Mappings({
            @Mapping(target = "userName", source = "user.name"),
            @Mapping(target = "userPhoneNumber", source = "user.phoneNumber")
    })
    Booking dtoToEntity(BookingItem bookingItem);

    @Mappings({
            @Mapping(target = "user.name", source = "userName"),
            @Mapping(target = "user.phoneNumber", source = "userPhoneNumber"),
            @Mapping(target = "club.id", source = "clubId")
    })
    BookingItem entityToDto(Booking booking);
}
