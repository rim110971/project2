package com.platform.projapp.service;

import com.platform.projapp.dto.BookingItem;
import com.platform.projapp.model.Booking;
import com.platform.projapp.model.Club;
import com.platform.projapp.model.User;
import com.platform.projapp.repository.BookingRepository;
import com.platform.projapp.service.mappers.BookingMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookingService {
    private final BookingRepository bookingRepository;
    private final TableService tableService;
    private final ScheduleService scheduleService;
    private final ClubService clubService;
    private final BookingMapper bookingMapper;

    @Transactional
    public void makeClientBooking(User user, BookingItem bookingitem, Club club) throws ProcessingException {
        var booking = bookingMapper.dtoToEntity(bookingitem);
        booking.setClubId(club.getId());
        var clubWorks = assertClubWorks(booking, club);
        var hasFreeTables = assertClubHasFreeTables(booking, club);
        if (!clubWorks) {
            throw new ProcessingException("Невозможно сделать бронь в нерабочее время!");
        } else if (!hasFreeTables) {
            throw new ProcessingException("Нет свободных столов");
        } else {
            booking.setUserName(user.getName());
            booking.setUserPhoneNumber(user.getPhoneNumber());
            booking.setUserId(user.getId());
            bookingRepository.save(booking);
        }
    }

    @Transactional
    public List<BookingItem> makeClubBooking(User admin, BookingItem bookingitem, Club club) throws ProcessingException {
        var booking = bookingMapper.dtoToEntity(bookingitem);
        booking.setClubId(club.getId());
        var clubWorks = assertClubWorks(booking, club);
        var hasFreeTables = assertClubHasFreeTables(booking, club);
        if (!clubWorks) {
            throw new ProcessingException("Невозможно сделать бронь в нерабочее время!");
        } else if (!hasFreeTables) {
            throw new ProcessingException("Нет свободных столов");
        } else {
            booking.setUserId(admin.getId());
            bookingRepository.save(booking);
            return getBookingByClubId(club.getId(), booking.getDate());
        }
    }

    public List<BookingItem> getBookingByUserId(Long userId) {
        return bookingRepository
                .getAllBookingByUserId(userId, Sort.by("date", "startTime").descending())
                .stream()
                .map(b -> {
                    var bookItem = bookingMapper.entityToDto(b);
                    var title = clubService
                            .findById(Long.parseLong(bookItem.getClub().getId()))
                            .getTitle();
                    bookItem.getClub().setTitle(title);
                    return bookItem;
                })
                .collect(Collectors.toList());
    }

    public List<BookingItem> getBookingByClubId(Long clubId, LocalDate date) {
        return bookingRepository
                .getAllBookingByClubIdAndDate(clubId, date, Sort.by("date", "startTime").descending())
                .stream()
                .map(bookingMapper::entityToDto)
                .collect(Collectors.toList());
    }

    public Booking getBookingById(Long id) { return bookingRepository.findById(id).orElse(null); }

    public void cancelBooking(Long id) {
        bookingRepository.deleteById(id);
    }

    private boolean assertClubWorks(Booking booking, Club club) throws ProcessingException {
        return scheduleService.containsScheduleForBooking(booking, club);
    }

    private boolean assertClubHasFreeTables(Booking booking, Club club) throws ProcessingException {
        var tablesCount = tableService.getClubInventory(club.getId()).get(booking.getVariant());
        var busyTablesCount = bookingRepository.getCountBusyTables(
                club.getId(),
                booking.getDate(),
                booking.getStartTime(),
                booking.getEndTime(),
                booking.getVariant()
        ).orElse(0);
        return busyTablesCount + booking.getCount() <= tablesCount;
    }
}
