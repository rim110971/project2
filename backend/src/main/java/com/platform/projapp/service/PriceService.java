package com.platform.projapp.service;

import com.platform.projapp.model.BilliardTypes;
import com.platform.projapp.model.Price;
import com.platform.projapp.model.PriceId;
import com.platform.projapp.model.Schedule;
import com.platform.projapp.repository.PriceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class PriceService {
    private final PriceRepository priceRepository;

    public void addPricing(Map<BilliardTypes, Integer> pricing, Long scheduleId) {
        List<Price> prices = new ArrayList<>();
        for (var item : pricing.entrySet()) {
            prices.add(new Price(
                    new PriceId(scheduleId, item.getKey()),
                    item.getValue()
            ));
        }
        priceRepository.saveAll(prices);
    }

    public Map<BilliardTypes, Integer> getPricingBySchedule(Long scheduleId) {
        var pricing = new HashMap<BilliardTypes, Integer>();
        for (var item : priceRepository.findAllByScheduleId(scheduleId)) {
            var type = item.getId().getBilliardType();
            var price = item.getPrice();
            pricing.put(type, price);
        }
        return pricing;
    }
}
