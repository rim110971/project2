package com.platform.projapp.service;

public class ProcessingException extends Exception {
    public ProcessingException(String message) {
        super(message);
    }
}
