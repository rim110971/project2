package com.platform.projapp.service.mappers;

import com.platform.projapp.dto.request.ScheduleItemRequest;
import com.platform.projapp.model.Schedule;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.time.DayOfWeek;
import java.util.*;

@Mapper(componentModel = "spring")
public interface ScheduleMapper {
    @Mappings({
            @Mapping(target = "weekday", source = "daysOfWeek"),
            @Mapping(target = "startTime", source = "item.startTime"),
            @Mapping(target = "endTime", source = "item.endTime")
    })
    Schedule dtoToEntity(ScheduleItemRequest request);

    default Integer map(List<DayOfWeek> weekdays) {
        Integer result = weekdays.size() > 0 ? 0 : null;
        for (DayOfWeek weekday : weekdays) {
            switch (weekday) {
                case MONDAY -> result += 1;
                case TUESDAY -> result += 2;
                case WEDNESDAY -> result += 4;
                case THURSDAY -> result += 8;
                case FRIDAY -> result += 16;
                case SATURDAY -> result += 32;
                case SUNDAY -> result += 64;
            }
        }
        return result;
    }

    default List<DayOfWeek> map(Integer value) {
        int[] numbers = {1, 2, 4, 8, 16, 32, 64};
        List<DayOfWeek> weekdays = new ArrayList<>();
        for (int i = 0; i < numbers.length; i++) {
            if ((value & numbers[i]) == numbers[i]) {
                weekdays.add(DayOfWeek.values()[i]);
            }
        }
        return weekdays;
    }
}
