package com.platform.projapp.service;

import com.platform.projapp.dto.request.ScheduleItemRequest;
import com.platform.projapp.dto.response.ClubScheduleResponseItem;
import com.platform.projapp.model.Booking;
import com.platform.projapp.model.Club;
import com.platform.projapp.model.Schedule;
import com.platform.projapp.repository.ScheduleRepository;
import com.platform.projapp.service.mappers.ScheduleMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ScheduleService {
    private final ScheduleMapper scheduleMapper;
    private final ScheduleRepository scheduleRepository;
    private final PriceService priceService;

    public List<ClubScheduleResponseItem> getScheduleOfClub(Club club) {
        var schedules = scheduleRepository.findAllByClubId(club.getId());
        var data = new ArrayList<ClubScheduleResponseItem>();
        for (var schedule : schedules) {
            data.addAll(getResponseItem(schedule.getId()));
        }
        return data;
    }

    public List<ClubScheduleResponseItem> addScheduleItem(ScheduleItemRequest request, Club club) {
        Schedule schedule = scheduleMapper.dtoToEntity(request);
        schedule.setClubId(club.getId());
        schedule = scheduleRepository.save(schedule);
        priceService.addPricing(request.getItem().getPricing(), schedule.getId());
        return getScheduleOfClub(club);
    }

    public List<ClubScheduleResponseItem> getResponseItem(Long scheduleId) {
        var schedule = scheduleRepository.findById(scheduleId).orElse(null);
        if (schedule == null) {
            return null;
        }
        var pricing = priceService.getPricingBySchedule(schedule.getId());
        var data = new ArrayList<ClubScheduleResponseItem>();
        for (DayOfWeek weekday : decodingWeekdays(schedule.getWeekday())) {
            var shiftDays = weekday.getValue() - LocalDate.now().getDayOfWeek().ordinal() - 1;
            data.add(new ClubScheduleResponseItem(
                    schedule.getId().toString(),
                    LocalDate.now().plusDays(shiftDays).atTime(schedule.getStartTime()),
                    LocalDate.now().plusDays(shiftDays).atTime(schedule.getEndTime()),
                    pricing,
                    decodingWeekdays(schedule.getWeekday())
                            .stream()
                            .map(DayOfWeek::ordinal)
                            .collect(Collectors.toList())

            ));
        }
        return data;
    }

    public boolean containsScheduleForBooking(Booking booking, Club club) {
        var schedules = scheduleRepository.findAllByBookingTime(
                booking.getStartTime(),
                booking.getEndTime()
        );
        var number = (int)Math.pow(2, booking.getDate().getDayOfWeek().getValue() - 1);
        for (var schedule : schedules) {
            if ((schedule.getWeekday() & number) == number) {
                return true;
            }
        }
        return false;
    }

    private List<DayOfWeek> decodingWeekdays(Integer value) {
        int[] numbers = {1, 2, 4, 8, 16, 32, 64};
        List<DayOfWeek> weekdays = new ArrayList<>();
        for (int i = 0; i < numbers.length; i++) {
            if ((value & numbers[i]) == numbers[i]) {
                weekdays.add(DayOfWeek.values()[i]);
            }
        }
        return weekdays;
    }
}
