package com.platform.projapp.service;

import com.platform.projapp.repository.ImageRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Slf4j
@Service
@RequiredArgsConstructor
public class ImageService {
    private final ImageRepository imageRepository;

    public byte[] getImageById(String id) {
        var imageDto = imageRepository.getById(id);
        var base64ImageWithPrefix = imageDto.getContent();
        return extractRawImageBytes(base64ImageWithPrefix);
    }

    private byte[] extractRawImageBytes(byte[] base64ImageWithPrefix) {
        return Base64.getDecoder().decode(new String(base64ImageWithPrefix).split(",")[1]);
    }
}
