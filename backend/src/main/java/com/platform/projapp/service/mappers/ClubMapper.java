package com.platform.projapp.service.mappers;

import com.platform.projapp.dto.ImageDTO;
import com.platform.projapp.dto.ProfileDTO;
import com.platform.projapp.dto.request.RegisterOrUpdateClubRequest;
import com.platform.projapp.model.Club;
import com.platform.projapp.model.Image;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

@Mapper(componentModel = "spring")
public interface ClubMapper {
    ProfileDTO clubToProfileDTO(Club club);

    Club dtoToEntity(RegisterOrUpdateClubRequest dto);

    Image dtoToEntity(ImageDTO dto);

    List<Image> dtosToEntitys(List<ImageDTO> dto);

    default byte[] stringToBytes(String str) {
        return str.getBytes(StandardCharsets.UTF_8);
    }

    default String bytesToString(byte[] bytes) {
        return new String(bytes);
    }

    default String map(List<String> value) {
        return String.join(";", value);
    }

    default List<String> map(String value) {
        return value == null ? List.of() : Arrays.stream(value.split(";")).toList();
    }
}
