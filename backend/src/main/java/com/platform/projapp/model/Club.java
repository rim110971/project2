package com.platform.projapp.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "clubs")
public class Club {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "club_id")
    private Long id;
    @Column(name = "title")
    private String title;
    @Column(name = "city")
    private String city;
    @Column(name = "address")
    private String address;
    @Column(name = "url")
    private String url;
    @Column(name = "phone")
    private String phoneNumber;
    @Column(name = "additional_services")
    private String additionalServices;
    @Column(name = "description")
    private String description;
    @Column
    @OneToMany(mappedBy = "clubId")
    private List<Image> photos;
    @Column(name = "avatar")
    private String avatar;

    @OneToOne
    @JoinColumn(name = "admin_id")
    private User admin;

    public Long getAdminId() {
        return admin.getId();
    }
}