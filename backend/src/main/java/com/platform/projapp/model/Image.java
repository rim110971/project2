package com.platform.projapp.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "image")
public class Image {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "club_id")
    private Long clubId;

    @Column(name = "content")
    private byte[] content;
}
