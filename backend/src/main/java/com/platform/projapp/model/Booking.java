package com.platform.projapp.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "bookings")
public class Booking {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "start_time")
    private LocalTime startTime;
    @Column(name = "end_time")
    private LocalTime endTime;
    @Column(name = "date")
    private LocalDate date;
    @Column(name = "count")
    private Integer count;
    @Column(name = "variant")
    private BilliardTypes variant;
    @Column(name = "user_id")
    private Long userId; // При создании брони админом, здесь имя админа клуба, а не пользователя
    @Column(name = "user_name")// Поэтому данные "настоящего" пользователя храним отдельно
    private String userName;
    @Column(name = "user_phone_number")
    private String userPhoneNumber;
    @Column(name = "club_id")
    private Long clubId;
}
