package com.platform.projapp.model;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author Yarullin Renat
 */

public enum AccessRole implements GrantedAuthority {
    ROLE_CLIENT,
    ROLE_CLUB;

    @Override
    public String getAuthority() {
        return name();
    }
}
