package com.platform.projapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserShortBody {
    private Long id;
    private String email;
    private String name;
    private String phoneNumber;
}
