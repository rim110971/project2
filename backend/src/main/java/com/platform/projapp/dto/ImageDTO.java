package com.platform.projapp.dto;

import lombok.Data;

@Data
public class ImageDTO {
    private String id;
    private boolean avatar;
    private String content;
}
