package com.platform.projapp.dto;

import com.platform.projapp.model.BilliardTypes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProfileDTO {
    private String email;
    private String name;
    private String password;
    private String newPassword;
    private String phoneNumber;
    private String title;
    private String city;
    private String address;
    private String description;
    private List<String> additionalServices;
    private Map<BilliardTypes, Long> inventory;
    private List<ImageDTO> photos;
}
