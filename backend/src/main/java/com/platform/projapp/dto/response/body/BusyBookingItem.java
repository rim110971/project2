package com.platform.projapp.dto.response.body;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class BusyBookingItem {
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private String label;
}
