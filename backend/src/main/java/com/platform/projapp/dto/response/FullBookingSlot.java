package com.platform.projapp.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FullBookingSlot {
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private String label;
    private String colour;
}
