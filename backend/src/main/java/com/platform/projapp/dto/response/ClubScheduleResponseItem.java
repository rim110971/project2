package com.platform.projapp.dto.response;

import com.platform.projapp.model.BilliardTypes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClubScheduleResponseItem {
    private String id;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private Map<BilliardTypes, Integer> pricing;
    private List<Integer> daysOfWeek; // Начиная с 0 (ПН)
}
