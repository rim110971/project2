package com.platform.projapp.dto.response.body;

import com.platform.projapp.model.Club;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
public class ClubResponseShortBody implements ResponseBody {
    private Long id;
    private String title;
    private String address;
    private Integer priceMean;
    private String avatar;

    public ClubResponseShortBody(Club club, Integer priceMean) {
        this.id = club.getId();
        this.title = club.getTitle();
        this.address = club.getAddress();
        this.priceMean = priceMean;
        this.avatar = club.getAvatar();
    }
}
