package com.platform.projapp.dto.response.body;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class ScheduleResponseBody implements ResponseBody {
    private Long id;
    private List<Integer> daysOfWeek;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private Map<String, Integer> pricing;
}
