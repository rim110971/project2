package com.platform.projapp.dto.request;

import com.platform.projapp.dto.ScheduleItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.DayOfWeek;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScheduleItemRequest {
    private Long id;
    private List<DayOfWeek> daysOfWeek;
    private ScheduleItem item;
}
