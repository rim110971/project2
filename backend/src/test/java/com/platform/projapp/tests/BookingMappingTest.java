package com.platform.projapp.tests;

import com.platform.projapp.dto.BookingItem;
import com.platform.projapp.dto.UserShortBody;
import com.platform.projapp.model.BilliardTypes;
import com.platform.projapp.model.Booking;
import com.platform.projapp.service.mappers.BookingMapper;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class BookingMappingTest {
    private final BookingMapper bookingMapper = Mappers.getMapper(BookingMapper.class);


    @Test
    public void testBookMapping() {
        var bookingDto = BookingItem.builder()
                .id(1L)
                .startTime(LocalTime.now())
                .endTime(LocalTime.now().plusHours(1))
                .variant(BilliardTypes.RUSSIAN)
                .date(LocalDate.now())
                .user(new UserShortBody(1L, "email", "name", "phone"))
                .count(1).build();
        var booking = bookingMapper.dtoToEntity(bookingDto);
//        assertNotNull(booking.getClubId());
        assertNotNull(booking.getCount());
        assertNotNull(booking.getDate());
        assertNotNull(booking.getEndTime());
        assertNotNull(booking.getStartTime());
        assertNotNull(booking.getUserPhoneNumber());
        assertNotNull(booking.getUserName());
        assertNotNull(booking.getVariant());
//        assertNotNull(booking.getUserId());
    }

    @Test
    public void testBookMappingWithoutUser() {
        var bookingDto = BookingItem.builder()
                .id(1L)
                .startTime(LocalTime.now())
                .endTime(LocalTime.now().plusHours(1))
                .variant(BilliardTypes.RUSSIAN)
                .date(LocalDate.now())
//                .user(new UserShortBody(1L, "email", "name", "phone"))
                .count(1).build();
        var booking = bookingMapper.dtoToEntity(bookingDto);
//        assertNotNull(booking.getClubId());
        assertNotNull(booking.getCount());
        assertNotNull(booking.getDate());
        assertNotNull(booking.getEndTime());
        assertNotNull(booking.getStartTime());
        assertNotNull(booking.getVariant());
//        assertNotNull(booking.getUserId());
    }

    @Test
    public void testDtoToEntityMapping() {
        var bookingDto = Booking.builder()
                .id(1L)
                .startTime(LocalTime.now())
                .endTime(LocalTime.now().plusHours(1))
                .variant(BilliardTypes.RUSSIAN)
                .date(LocalDate.now())
                .userName("name")
                .clubId(1l)
                .userPhoneNumber("phone")
//                .user(new UserShortBody(1L, "email", "name", "phone"))
                .count(1).build();
        var booking = bookingMapper.entityToDto(bookingDto);
//        assertNotNull(booking.getClubId());
        assertNotNull(booking.getCount());
        assertNotNull(booking.getDate());
        assertNotNull(booking.getEndTime());
        assertNotNull(booking.getStartTime());
        assertNotNull(booking.getVariant());
        assertNotNull(booking.getUser().getPhoneNumber());
        assertNotNull(booking.getUser().getName());
        assertNotNull(booking.getClub().getId());
//        assertNotNull(booking.getUserId());
    }
}
